import hashlib
import time
import csv 
t0 = time.time()
a = open ("FIO.txt", "r")
z = a.read()
print(z)
# Получение ХЭШ файла
def hash_file(filename):
   
    h = hashlib.sha1()
    with open (filename, "rb") as f:
        text = f.read()
        h.update(text)
    
    return h.hexdigest()

# ХЭШ ФИО в 2м коде

def hash_bi_FIO(filename):
    h = hashlib.sha1()
    with open(filename, "r") as f:
        a = f.readline()
        z = "".join(format(ord(x),"b") for x in a)
        # z = z[:2]+"0"+z[3:]
        # Для изменения бита раскоментировать
        print(z)
        k = z.encode()
        h.update(k)
    return h.hexdigest()
# Для сохранения в csv формате и получения времени выполнения программы
#res = open("first.csv","a+")
#message = hash_file("FIO.txt")
#message3 = hash_bi_FIO("FIO.txt")
#res.write(message + "\n")
#res.write(message3 + "\n")
#res.close()
#t1 = time.time() - t0
#print("Время выполнения программы: ", t1)



